﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;

namespace Cracka.MessageMover
{
    class Program
    {
        static readonly string AWS_ACCOUNT = "https://sqs.ap-southeast-2.amazonaws.com/167157044207/";

        static void Main(string[] args)
        {
            if (args.Length != 3)
            {
                Console.WriteLine("Invalid number of arguments. Must have 3 arguments passed. Environment SourceQueue DestinationQueue");
                throw new Exception("Invalid number of arguments. Must have 3 arguments passed. Environment SourceQueue DestinationQueue");
            }

            var environment = args[0];
            var sourceQueue = args[1];
            var destinationQueue = args[2];

            var fixedUpSourceURL = string.Format("{0}{1}-{2}", AWS_ACCOUNT,environment,sourceQueue);
            var fixedUpDestinationURL = string.Format("{0}{1}-{2}", AWS_ACCOUNT, environment, destinationQueue); ;

            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest();
            receiveMessageRequest.QueueUrl = fixedUpSourceURL;

            try
            {
                IAmazonSQS sqs = AWSClientFactory.CreateAmazonSQSClient(RegionEndpoint.APSoutheast2);
                Console.WriteLine(string.Format("Sending message to {0} from {1}", destinationQueue, sourceQueue));
                while (true)
                {
                    ReceiveMessageResponse receiveMessageResponse = sqs.ReceiveMessage(receiveMessageRequest);
                    
                    if (receiveMessageResponse.Messages.Count() == 0)
                    {
                        Console.WriteLine("Programing ending. All messages have been sent");
                        break;
                    }

                    Console.WriteLine(string.Format("Message Count: {0}",receiveMessageResponse.Messages.Count()));
                   
                    foreach (Message message in receiveMessageResponse.Messages)
                    {
                        Console.WriteLine("Sending the message");
                        SendMessageRequest sendMessageRequest = new SendMessageRequest();
                        sendMessageRequest.QueueUrl = fixedUpDestinationURL;
                        sendMessageRequest.MessageBody = message.Body;
                        sqs.SendMessage(sendMessageRequest);
                        Console.WriteLine("Message sent successfully");

                        Console.WriteLine("Deleting the message");
                        DeleteMessageRequest deleteRequest = new DeleteMessageRequest();
                        deleteRequest.QueueUrl = fixedUpSourceURL;
                        deleteRequest.ReceiptHandle = message.ReceiptHandle;
                        sqs.DeleteMessage(deleteRequest);
                        Console.WriteLine("Message deleted successfully");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("There was an error polling the specified source queue. Make sure the queue name has been typed correctly. {0}", e.Message));
            }
        }
    }
}
















































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































